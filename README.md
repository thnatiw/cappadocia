When i finished unversity i always wanted to provide an english translation of my master thesis on the subject of Cappadocia. As i had some copyrighted material in it i also wanted to provide some unencumbered assests to my orginal version. 
I managed to do a quick translation of the original german text and somehow came up with a rough draft but got carried away by all the other things i wanted to include. I started making a glossary and index which i did so wrong, that i don't want to do anything with it at all. 
I also tried to include a new map and wanted to provide an update lineage of the royal familes of Cappadocia but never could warp my head around the best way to show the entanglements of all characters. 

I still feel that maybe someone can benefit from the basic information i gathered on the subject (especially by the bibliography, which i frankly never updated from 2012 onwards and which should include a dozen or more new articles and books by now) and i do hope maybe someone else can make a much better job at doing a proper overview of a fascinating subject which includes hellenistic Cappadocia.

The provided TeX file compiles without errors using XeLaTeX. References might be off but it will create a full PDF from it.

I also include the assets i managed to create in the time i also did this.